// SPDX-License-Identifier: Unlicense
pragma solidity 0.8.17;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

/// @title NFT marketplace contract 
/// @author Tsafack Fritz <tsafack07albin@gmail.com>
/// @notice This contract is in charge of trading NFT
/// @dev This constract helps to create, update and sell NFTs
contract NFTMarketPlace is ERC721URIStorage{
    address payable owner;

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    Counters.Counter private _itemSold;

    uint256 listPrice = 0.00001 ether;

    constructor() ERC721("NFTMarketPlace", "NFTM"){
        owner = payable(msg.sender);
    }

    struct ListedToken{
        uint256 tokenId;
        address payable owner;
        address payable seller;
        uint256 price;
        bool currentlyListed;
    }

    mapping(uint256=>ListedToken) idToListedToken;

/// @notice Change the list price of the marketplace
/// @dev update the list price of the marketplace
/// @param _listPrice the new list price
    function updateListPrice(uint256 _listPrice) public payable{
        require(owner == msg.sender, "Only the owner can update the listing price");
        listPrice = _listPrice;
    }

/// @notice get the list price of the marketplace
/// @dev return the list price of the marketplace
/// @return uint256 listPrice the list price of the marketplace
    function getListPrice() public view returns(uint256){
        return listPrice;
    }

/// @notice get the recent token created
/// @dev return the last ListedToken element
/// @return ListedToken token the last ListedToken
    function getLatestIdToListedToken() public view returns(ListedToken memory){
        uint256 currentTokenId = _tokenIds.current();
        return idToListedToken[currentTokenId];
    }

/// @notice get the ListedToken of the given token id
/// @dev return the ListedToken of the id passed as parameter
/// @param tokenId the token id we want to get details
/// @return ListedToken the ListedToken with the id passed as parameter
    function getListedForTokenId(uint256 tokenId) public view returns(ListedToken memory){
        return idToListedToken[tokenId];
    }

/// @notice get the number of nfts
/// @dev return the number of nfts
/// @return uint256 the ListedToken with the id passed as parameter
    function getCurrentToken() public view returns (uint256){
        return _tokenIds.current();
    }

/// @notice change the selling status and the price of the given token id
/// @dev update the price and the currentlyListed status of the token id passed as parameter
/// @param tokenId the token id
/// @param currentlyListed the currently listed status
/// @param price the new token price
/// @return ListedToken the updated nft
    function updateTokenInfos(uint256 tokenId, bool currentlyListed, uint256 price) public returns(ListedToken memory){
        require(price > 0, "make sure price is positive");
        ListedToken storage token = idToListedToken[tokenId];

        require(msg.sender == token.seller, "Not authorized , seller only");
        token.price = price;
        token.currentlyListed = currentlyListed;
        return token;
    }

/// @notice creates the nft
/// @dev set the nft data
/// @param tokenURI the token uri ei the token metadata url
/// @param price the selling price of the nft
/// @return uint the created token id
    function createToken(string memory tokenURI, uint256 price) public payable returns(uint) {
        require(msg.value == listPrice, "Send enough ether to list");
        require(price > 0, "make sure price is positive");

        _tokenIds.increment();
        uint256 currentTokenId = _tokenIds.current();
        _safeMint(msg.sender, currentTokenId);

        _setTokenURI(currentTokenId, tokenURI);
        createListedToken(currentTokenId, price);

        return currentTokenId;
    }

/// @dev save the nft as listed and transfer the token to this contract
/// @param tokenId the token id
/// @param price the selling price of the nft
    function createListedToken(uint256 tokenId, uint256 price) private {
        idToListedToken[tokenId] = ListedToken(
            tokenId,
            payable(address(this)),
            payable(msg.sender),
            price,
            false
        );

        _transfer(msg.sender, address(this), tokenId);
    }
    
/// @notice get all selling NFT
/// @dev get all listed token with selling status
/// @return Array the array of all selling nfts
    function getAllNFTs() public view returns(ListedToken[] memory){
        uint nftCount = _tokenIds.current();
        ListedToken[] memory tokens = new ListedToken[](nftCount);

        uint currentIndex = 0;

        for(uint i=0; i<nftCount;i++){
            uint currentId = i+1;
            ListedToken storage currentItem = idToListedToken[currentId];
            tokens[currentIndex] = currentItem;
            currentIndex += 1;
        }

        return tokens;
    }
    
/// @notice get all NFT of the sender
/// @dev return all NFT of the sender
/// @return Array the array of all selling nfts
    function getMyNFTs() public view returns(ListedToken[] memory){
        uint totalItemCount =_tokenIds.current();
        uint itemCount = 0;
        uint currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if(idToListedToken[i+1].owner == msg.sender || idToListedToken[i+1].seller == msg.sender){
                itemCount +=1;
            }
        }
        
        ListedToken[] memory items = new ListedToken[](itemCount);
        for (uint256 i = 0; i < totalItemCount; i++) {
            if(idToListedToken[i+1].owner == msg.sender || idToListedToken[i+1].seller == msg.sender){
                uint currentId = i+1;
                ListedToken storage currentItem = idToListedToken[currentId];
                items[currentIndex] = currentItem;
                currentIndex +=1;
            }
        }

        return items;
    }
    
/// @notice execute the selling transaton of th given token id
/// @dev send the nft price to the seller and transfer the token to the sender
/// @param tokenId the token id we want to execute sale
    function executeSale(uint256 tokenId) public payable{
        uint price = idToListedToken[tokenId].price;
        
        require(msg.value == price, "Submit the asking price for the NFT in order to purchase");
        require(idToListedToken[tokenId].currentlyListed == true, "The nft is no more on sale");

        address seller = idToListedToken[tokenId].seller;
        idToListedToken[tokenId].currentlyListed = false;
        idToListedToken[tokenId].seller = payable(msg.sender);
        _itemSold.increment();
        _transfer(address(this), msg.sender, tokenId);

        approve(address(this), tokenId);

        payable(owner).transfer(listPrice);
        payable(seller).transfer(msg.value);
    }
}